//validation
const Joi = require('@hapi/joi');


const registerVal = data => {
    const schema = {
        userName: Joi.string().min(5).required(),
        password: Joi.string().min(6).required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
      };
    return Joi.validate(data, schema);
};


const loginVal = data => {
    const schema = {
        userName: Joi.string().min(6).required(),
        password: Joi.string().min(6).required(),
      };
    return Joi.validate(data,schema);
};


module.exports.registerVal = registerVal;
module.exports.loginVal = loginVal;
const express = require('express')
const mongoose = require('mongoose')
const User = require('../DB/User')
const UserProgress = require('../DB/UserProgress')
const route = express.Router();
const {registerVal, loginVal} = require('../validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')


// create new user with post request
route.post('/newUser', async(req,res)=>{
    //validoidaan rekisteröityminen, jotta jokainen vaadittu kenttä saadaan
    const { error } = registerVal(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //katotaan onko username tai password olemassa
    const userNameExists = await User.findOne({userName: req.body.userName});
    if(userNameExists) return res.status(400).send('Username already exists');

    //salasanan kryptaus
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    const{firstName,lastName,userName,password} = req.body;
    let user = {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.userName = userName;
    user.password = hashPassword;
    let userModel = new User(user);
    let createdUser = await userModel.save();
    res.json({_id: createdUser._id});

    // luodaan käyttäjälle pistetaulu jolla seurataan käyttäjän edistymistä
    let userProgress = {}
    userProgress._id = createdUser._id
    userProgress.totalPoints = 0
    userProgress.steps = 0
    userProgress.locationsFound = [
        {name:"Tapahtumapuiston puuveistos",found:false},
        {name:"Lahden matkakeskuksen alikulkutunnelin maskit",found:false},
        {name:"Fellmanninpuiston muistomerkki",found:false},
        {name:"Hakkapeliittapatsas",found:false},
        {name:"Hiihtäjäpatsas",found:false},
        {name:"Hyppyrimäet",found:false},
        {name:"Häränsilmä",found:false},
        {name:"Kirkkopuiston patsas",found:false},
        {name:"Lanupuisto",found:false},
        {name:"Jari Litmasen Patsas",found:false},
        {name:"Paasikiven Patsas",found:false},
        {name:"Perhepuiston skeittirampit",found:false},
        {name:"Radiomastot",found:false},
        {name:"Radiomuseon eläimet",found:false},
        {name:"Tiirismaan lenkkipolku",found:false},
        {name:"Vesiurut",found:false}
    ]

    userProgress.questions = "false"
    console.log(userProgress)
    let userProgressModel = new UserProgress(userProgress)
    await userProgressModel.save();
})

route.post('/login', async(req,res) => {
    const { error } = loginVal(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const userExists = await User.findOne({userName: req.body.userName});
    if(!userExists) return res.status(400).send('Username or password is invalid');
    //salasanan tarkistus
    const validPass = await bcrypt.compare(req.body.password, userExists.password);
    if(!validPass) return res.status(400).send('Username or password is invalid');

   
    //jsonwebtokenin luonti
    const token = jwt.sign({_id: userExists._id}, process.env.TOKEN_SECRET);
    res.header('auth_token', token).send(token);

})

// haetaan userProgress dataa tokenilla
route.get('/getData', (req, res, next) => {
    const header = req.headers['authorization'];

    if(typeof header !== 'undefined') {
        const bearer = header.split(' ');
        const token = bearer[1];

        req.token = token;
        next();
    } else {
        //If header is undefined return Forbidden (403)
        res.sendStatus(403)
    }
},(req, res, next) => {
//verify the JWT token generated for the user
jwt.verify(req.token, process.env.TOKEN_SECRET, async (err, authorizedData) => {
    if(err){
        //If error send Forbidden (403)
        console.log('ERROR: Could not connect to the protected route');
        res.sendStatus(403);
    } else {
        //If token is successfully verified, we can get users progress data 
        console.log('SUCCESS: Connected to protected route');
        const userExists = await UserProgress.findOne({_id: authorizedData._id});
        if(userExists) return res.json(userExists);
        else return res.status(400).send('User not found');
    }
})
})

// askeleiden lisäys
route.put('/updateSteps', (req, res, next) => {
    const header = req.headers['authorization'];

    if(typeof header !== 'undefined') {
        const bearer = header.split(' ');
        const token = bearer[1];

        req.token = token;
        next();
    } else {
        //If header is undefined return Forbidden (403)
        res.sendStatus(403)
    }
},(req, res, next) => {
//verify the JWT token generated for the user
jwt.verify(req.token, process.env.TOKEN_SECRET, async (err, authorizedData) => {
    if(err){
        //If error send Forbidden (403)
        console.log('ERROR: Could not connect to the protected route');
        res.sendStatus(403);
    } else {
        //If token is successfully verified, we can get users progress data 
        console.log('SUCCESS: Connected to protected route');
        const userExists = await UserProgress.findOne({_id: authorizedData._id});
        if(userExists){
            userExists.steps += req.body.stepCount
            userExists.totalPoints += req.body.stepCount
            await userExists.save();
            return res.json(userExists);
        }
        else return res.status(400).send('User not found');
    }
})
})

// new location found
route.put('/locationFound', (req, res, next) => {
    const header = req.headers['authorization'];

    if(typeof header !== 'undefined') {
        const bearer = header.split(' ');
        const token = bearer[1];

        req.token = token;
        next();
    } else {
        //If header is undefined return Forbidden (403)
        res.sendStatus(403)
    }
},(req, res, next) => {
//verify the JWT token generated for the user
jwt.verify(req.token, process.env.TOKEN_SECRET, async (err, authorizedData) => {
    if(err){
        //If error send Forbidden (403)
        console.log('ERROR: Could not connect to the protected route');
        res.sendStatus(403);
    } else {
        //If token is successfully verified, we can get users progress data 
        console.log('SUCCESS: Connected to protected route');
        const userExists = await UserProgress.findOne({_id: authorizedData._id});
        if(userExists){
            console.log(req.body.location);
            userExists.locationsFound
            let elem = userExists.locationsFound.find(location => location.name === req.body.location)
            elem.found = true;
            userExists.totalPoints += 1000
            await userExists.save();
            return res.json(userExists);
        }
        else return res.status(400).send('User not found');
    }
})
})


// get all users
route.get('/allUsers', async(req,res)=>{
    User.find(function (err, usrs) {
            if (err) return console.error(err);
            res.json(usrs);
        })
})

module.exports = route;

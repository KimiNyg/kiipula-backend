const mongoose = require('mongoose')

const userProgress = new mongoose.Schema({
    _id:{
        type:mongoose.Schema.Types.ObjectId,
        required:true,
    },
    totalPoints:{
        type:Number,
        required:true,
    },
    steps:{
        type:Number,
        required:true,
    },
    locationsFound:{
        type:[
                {name:{type:String,required:true},
                found:{type:Boolean,required:true}}
        ],
        required:true
    },
    questions:{
        type:String,
        required:true,
    }
})

module.exports = UserProgress = mongoose.model('userProgress',userProgress)
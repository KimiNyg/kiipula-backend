const express = require('express')
var cors = require('cors')
const connectDB = require('./DB/Connection')

const app = express()


app.use(cors())
const port = 3000

// connetcting to MongoDB Atlas
connectDB()

app.use(express.json({extended:false}))
app.use('/api/userModel', require('./Api/User'))

app.listen(port, () => {
  console.log(`Kiipula backend app listening at http://localhost:${port}`)
})


const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config();
// creating the connection to the db
const connectDB = async()=>{
    await mongoose.connect(process.env.DB_CONNECT,{
        useUnifiedTopology: true,
        useNewUrlParser: true })
    console.log('connected to db!')
}

module.exports = connectDB